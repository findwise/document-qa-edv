import random
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(1, 10)

    @task
    def get_searches(self):
        self.client.get("/suggestions?sentence=dofinansowanie okularów", name="/suggestions")

