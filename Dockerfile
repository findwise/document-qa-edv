FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

COPY ./requirements.txt ./requirements.txt

RUN apt-get update && apt-get install -y aspell-pl && apt-get install -y enchant

RUN pip install --retries 0 --user --no-warn-script-location -r requirements.txt && \
  pip freeze

RUN pip install fastapi uvicorn

RUN export NUMEXPR_MAX_THREADS=4

COPY ./app /app