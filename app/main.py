import uvicorn
from fastapi import FastAPI
from nltk import download
from starlette.middleware.cors import CORSMiddleware

from embeddinggenerators.GloVe_embedding_generator import GloVeEmbeddingGenerator
from logic.elastic_connection import ElasticConnection
from logic.suggestion_generator import SuggestionGenerator
import torch

torch.cuda.empty_cache()
app = FastAPI()
stacked_emb_gen = GloVeEmbeddingGenerator()
elastic = ElasticConnection()
suggestion_generator = SuggestionGenerator(stacked_emb_gen, elastic)

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

download('punkt')

@app.get("/suggestions")
async def get_suggestions(
        q: str,
        num_of_suggestions: int = 20,
):
    res = await suggestion_generator.find_closest_word(
        q,
        num_of_suggestions
    )

    return res

# if __name__ == "__main__":
#     # pandarallel.initialize(nb_workers=6, progress_bar=True)
#     # stacked_emb_gen = BertEmbeddingGenerator()
#     stacked_emb_gen = GloVeEmbeddingGenerator()
#     elastic = ElasticConnection()
#     suggestion_generator = SuggestionGenerator(stacked_emb_gen, elastic)
#     uvicorn.run(app, host='0.0.0.0', port=8080)
