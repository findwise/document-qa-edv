from functools import lru_cache

import enchant
import stanza
import torch
from nltk import edit_distance

from logic.elastic_connection import ElasticConnection
from utils.extra_mappings import EXTRA_MAPPINGS
from utils.polish_stopwords import POLISH_STOPWORDS


class SuggestionGenerator:

    def __init__(self, emb_gen, elastic_connection: ElasticConnection):
        self.es = elastic_connection
        self.emb_gen = emb_gen
        stanza.download('pl')
        self.pipeline = stanza.Pipeline(lang='pl', processors='tokenize, pos, lemma')
        self.pl_dict = enchant.request_dict("pl")
        self.en_dict = enchant.request_dict("en_US")

    @torch.no_grad()
    async def find_closest_word(self, sentence, num_of_suggestions):
        processed = self.process_query(sentence)
        joined = ' '.join(processed)
        print(joined)
        current_emb = self.emb_gen.generate_embedding_torch(joined)
        return await self.es.find_closest_word(joined, sentence, current_emb, num_of_suggestions)

    @lru_cache(maxsize=1000)
    def process_query(self, sentence):
        sentence_lemma = []
        sentence = sentence.lower()
        for key in EXTRA_MAPPINGS:
            sentence = sentence.replace(key, EXTRA_MAPPINGS[key])
        doc = self.pipeline(sentence)
        for sent in doc.sentences:
            for word in sent.words:
                word_in_en_dict = self.en_dict.check(word.text)
                if self.pl_dict.check(word.lemma) and not word_in_en_dict:
                    sentence_lemma.append(word.lemma)
                elif word_in_en_dict:
                    sentence_lemma.append(word.text)
                else:
                    sentence_lemma.append(word.text)
        print(' '.join([word for word in sentence_lemma if word not in POLISH_STOPWORDS]))
        return [word for word in sentence_lemma if word not in POLISH_STOPWORDS]

    def spellcheck(self, word):
        suggestions = self.pl_dict.suggest(word)
        distances = [edit_distance(word, s) for s in suggestions]
        min_dist = min(distances)
        if min_dist / len(word) < 0.34:
            return suggestions[distances.index(min_dist)]
        else:
            return word
