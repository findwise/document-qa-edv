from enum import Enum

class SuggestionSort(Enum):
    HITS = 'hits'
    DELTA = 'delta'