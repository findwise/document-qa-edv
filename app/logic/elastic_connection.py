from elasticsearch import Elasticsearch


class ElasticConnection:

    def __init__(self):
        self.es = Elasticsearch(hosts=[{"host": "es03", "port": 9200}])

    async def find_closest_word(self, query, org_query, embeddings, num_of_suggestions=20):
        q = self.generate_scripted_query(
            {
                "dis_max": {
                    "queries": [
                        {
                            "match": {
                                "processesed_search": {
                                    "query": query,
                                    "minimum_should_match": "50%"
                                },
                            }
                        },
                        {
                            "match_phrase": {
                                "filename": {
                                    "query": org_query
                                }
                            }
                        },
                    ]
                }
            },
            query,
            org_query,
            num_of_suggestions,
            embeddings
        )

        res = self.es.search(body=q, index="documents")
        if res['hits']['total']['value'] == 0:
            backup_q = self.generate_scripted_query(
                {
                    "match_all": {}
                },
                query,
                org_query,
                num_of_suggestions,
                embeddings
            )
            res = self.es.search(body=backup_q, index="documents")
        return res

    def generate_scripted_query(self, query_template, query_string, org_query, num_of_suggestions, embeddings):
        q_vec_1 = embeddings[:100]
        q_vec_2 = embeddings[100:2148]
        q_vec_3 = embeddings[2148:4196]
        h_q = {"fields": {
            "snippet": {
                "fragment_size": 600,
                "number_of_fragments": 1,
                "highlight_query": {
                    "match": {
                        "snippet": {
                            "query": ' '.join(set(query_string.split(' ') + org_query.split(' '))),
                            "fuzziness": "AUTO"
                        }
                    }
                }
            }
        }}
        q = {
            "size": num_of_suggestions,
            "_source": ["search", 'snippet', 'filename'],
            "query": {
                "script_score": {
                    "query": query_template,
                    "script": {
                        "source": "1 / (1 + (l2norm(params.queryVector1, 'emb1') + l2norm(params.queryVector2, 'emb2') + "
                                  "l2norm(params.queryVector3, 'emb3'))/3)",
                        "params": {
                            "queryVector1": q_vec_1,
                            "queryVector2": q_vec_2,
                            "queryVector3": q_vec_3

                        }
                    }
                }
            },
            "collapse": {
                "field": "filename.keyword",
                "inner_hits": {
                    "name": "matching_snippets",
                    "size": 5,
                    "_source": ["search", 'snippet'],
                    "highlight": h_q
                }
            },
            "highlight": h_q,
            "suggest": {
                "text": org_query,
                "term_suggester":
                    {
                        "term": {"field": "search"}
                    }
            }
        }
        return q
