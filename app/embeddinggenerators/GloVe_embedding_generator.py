from functools import lru_cache

from flair.embeddings import WordEmbeddings, FlairEmbeddings, StackedEmbeddings, DocumentPoolEmbeddings
from flair.data import Sentence
import numpy as np
import torch
from embeddinggenerators.embedding_generator import EmbeddingGenerator


class GloVeEmbeddingGenerator(EmbeddingGenerator):
    def __init__(self):
        word_embedding = WordEmbeddings('glove_pl.gensim')
        # init Flair forward and backwards embeddings
        flair_embedding_forward = FlairEmbeddings('pl-forward')
        flair_embedding_backward = FlairEmbeddings('pl-backward')
        self.stop_words = list()
        self.stacked_embeddings = DocumentPoolEmbeddings([
                                                word_embedding,
                                                flair_embedding_forward,
                                                flair_embedding_backward,
                                               ])
        self.cuda = torch.cuda.is_available()

    def generate_embedding(self, document):
        sentence = Sentence(document)
        self.stacked_embeddings.embed(sentence)

        # return np.mean([np.array(token.embedding) for token in sentence], axis=0)
        return np.array(sentence.get_embedding().tolist())

    @lru_cache(maxsize=1000)
    def generate_embedding_torch(self, document):
        sentence = Sentence(document)
        self.stacked_embeddings.embed(sentence)

        # return np.mean([np.array(token.embedding) for token in sentence], axis=0)
        return sentence.get_embedding().to(dtype=torch.half).cpu().data.numpy() if self.cuda else \
            sentence.get_embedding().to(dtype=torch.float32).detach().numpy()